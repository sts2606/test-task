let list = document.getElementById('list');
const sortFunction = (a,b) => {
    if(a.nodetype == b.nodetype) {
        return a.nodename > b.nodename ? 1 : -1;
    }
    return a.nodetype > b.nodetype ? 1 : -1;
}
const getData = async (url) => {
    try{
        let response = await fetch(url);
        let data = await response.json();
        let tempArr = [];
            for (const item of data) {
                let dir = null;
                if(item.nodetype === 'dir'){
                    dir = item;
                    let index = data.indexOf(dir);
                    tempArr.push(data.splice(index, 1)[0]);
                };
            };
            data.sort(sortFunction);
            tempArr.sort(sortFunction);
            let finishArr = tempArr.concat(data);
            finishArr.forEach(element => {
                let li = document.createElement("li");
                li.innerHTML = `${JSON.stringify(element, null, ' ')}`;
                list.append(li);
            });
        } catch(error) {
            console.log(error);
        };
    };
    
    getData()


    let form = document.getElementById('form')
    console.log(form);
    form.addEventListener('submit', async(e) =>{
        e.preventDefault();
        let response = await fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: new FormData(form)
        });
        let result = await response.json();
        console.log(result);
    });